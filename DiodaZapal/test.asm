.nolist ;wy�acza nastepne wiersze z procesu sporzadzania raportu z kompilacji
.include "m8def.inc" ;duzy plik - dlategto nolisl; w przypadku inncyh mikrokontroler�w wysarczy zmienic ten iclude na np. m16def.inc
.list ;odwrotnosc nolist

.cseg
.org 0

sbi DDRB, 0 ;ustaw lini� 0 w porcie PB (PB0) na wyj�ciow� (nadaj warto�� 1)
sbi DDRB, 1 ;ustaw lini� 1 w porcie PB (PB1) na wyj�ciow� (nadaj warto�� 1)

sbi PORTB, 0 ;ustaw wysoki stan (+) na lini 0 portu PB
cbi PORTB, 1 ;ustaw niski stan (-) na lini 1 portu PB

;ldi R16, 0b00000011 ;zapisz do rejestu roboczego R16
;out DDRB, R16 ;przepisz warto�� R16 do DDRB (rejestr kierunku pracy lini 1-out, 0-in) - nie mo�na bezpo�rednio pisa� do rejestru funkcyjnego (R0-R15)
;ldi R16, 0b00000001 ;zapisz do rejestu roboczego R16
;out PORTB, R16 ;zapisz ustawienia napi�cia (1 -niskie, 0-wysokie)

petla:
  rjmp petla
