.nolist ;wyłacza nastepne wiersze z procesu sporzadzania raportu z kompilacji
.include "m8def.inc" ;duzy plik - dlategto nolisl; w przypadku inncyh mikrokontrolerów wysarczy zmienic ten iclude na np. m16def.inc
.list ;odwrotnosc nolist

.cseg
.org 0

ldi R16, 0b00000011 ;zapisz do rejestu roboczego R16
out DDRB, R16 ;przepisz wartość R16 do DDRB (rejestr kierunku pracy lini 1-out, 0-in) - nie można bezpośrednio pisać do rejestru funkcyjnego (R0-R15)
ldi R16, 0b00000001 ;zapisz do rejestu roboczego R16
out PORTB, R16 ;zapisz ustawienia napięcia (1 -niskie, 0-wysokie)

petla:
  rjmp petla